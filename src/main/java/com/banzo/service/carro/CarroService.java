package com.banzo.service.carro;

import com.banzo.domain.Carro;
import com.banzo.requests.carro.CarroPostRequestBody;

import java.util.List;

public interface CarroService {

    Carro salvarCarro(CarroPostRequestBody carroPostRequestBody);

    List<Carro> salvarCarros(List<CarroPostRequestBody> carrosPostRequestBody);
}
