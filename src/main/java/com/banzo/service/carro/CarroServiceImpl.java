package com.banzo.service.carro;

import com.banzo.domain.Carro;
import com.banzo.domain.Usuario;
import com.banzo.repository.CarroRepository;
import com.banzo.requests.carro.CarroPostRequestBody;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarroServiceImpl implements CarroService {


    private final CarroRepository carroRepository;

    public CarroServiceImpl(CarroRepository carroRepository) {
        this.carroRepository = carroRepository;
    }

    @Override
    public Carro salvarCarro(CarroPostRequestBody carroPostRequestBody) {
        return null;
    }

    @Override
    public List<Carro> salvarCarros(List<CarroPostRequestBody> carrosPostRequestBody) {
        List<Carro> carros = convertCarrosResquestToCarrosEntity(carrosPostRequestBody);
        carroRepository.saveAll(carros);
        return carros;
    }

    private List<Carro> convertCarrosResquestToCarrosEntity(List<CarroPostRequestBody> carrosPostRequestBody) {
        return carrosPostRequestBody.stream().map(this::convertCarroResquestToCarroEntity).collect(Collectors.toList());
    }

    private Carro convertCarroResquestToCarroEntity(CarroPostRequestBody carroPostRequestBody ){
        return Carro
                .builder()
                .color(carroPostRequestBody.getColor())
                .model(carroPostRequestBody.getModel())
                .year(carroPostRequestBody.getYear())
                .licensePlate(carroPostRequestBody.getLicensePlate())
                .usuario(Usuario.builder().id(carroPostRequestBody.getCodigoUsuario()).build())
                .build();
    }
}
