package com.banzo.service.usuario;

import com.banzo.domain.Usuario;
import com.banzo.repository.UsuarioRepository;
import com.banzo.requests.carro.UsuarioProjection;
import com.banzo.requests.usuario.UsuarioPostRequestBody;
import com.banzo.service.carro.CarroServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioServiceImpl implements UsuarioService {


    private final UsuarioRepository usuarioRepository;
    private final CarroServiceImpl carroService;

    public UsuarioServiceImpl(UsuarioRepository usuarioRepository, CarroServiceImpl carroService) {
        this.usuarioRepository = usuarioRepository;
        this.carroService = carroService;
    }

    @Override
    public List<Usuario> listarUsuarios() {
        return usuarioRepository.findAll();
    }

    @Override
    public Usuario salvarUsuario(UsuarioPostRequestBody usuarioPostRequestBody) {
        Usuario usuario = convertRequestToEntity(usuarioPostRequestBody);
        Usuario usuarioSalvo = usuarioRepository.save(usuario);
        usuarioPostRequestBody.getCars().forEach( c -> c.setCodigoUsuario(usuarioSalvo.getId()));
        carroService.salvarCarros(usuarioPostRequestBody.getCars());
        return null;
    }

    @Override
    public List<UsuarioProjection> buscarListaProjetada(String nome) {
        return usuarioRepository.getUsuarioByFirstName(nome);
    }

    private Usuario convertRequestToEntity(UsuarioPostRequestBody usuarioPostRequestBody) {
        return Usuario
                .builder()
                .birthday(usuarioPostRequestBody.getBirthday())
                .email(usuarioPostRequestBody.getEmail())
                .firstName(usuarioPostRequestBody.getFirstName())
                .lastName(usuarioPostRequestBody.getLastName())
                .phone(usuarioPostRequestBody.getPhone())
                .password(usuarioPostRequestBody.getPassword())
                .login(usuarioPostRequestBody.getLogin())
                .build();
    }


}
