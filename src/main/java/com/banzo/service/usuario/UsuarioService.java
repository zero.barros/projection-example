package com.banzo.service.usuario;

import com.banzo.domain.Usuario;
import com.banzo.requests.carro.UsuarioProjection;
import com.banzo.requests.usuario.UsuarioPostRequestBody;

import java.util.List;

public interface UsuarioService {

    List<Usuario> listarUsuarios();

    Usuario salvarUsuario(UsuarioPostRequestBody usuarioPostRequestBody);

    List<UsuarioProjection> buscarListaProjetada(String nome);
}
