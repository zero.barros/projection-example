package com.banzo.domain;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Carro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer year;

    private String licensePlate;

    private String model;

    private String color;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

}
