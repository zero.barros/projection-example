package com.banzo.controller;

import com.banzo.domain.Usuario;
import com.banzo.requests.carro.UsuarioProjection;
import com.banzo.requests.usuario.UsuarioPostRequestBody;
import com.banzo.service.usuario.UsuarioService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UsuarioController {

    private final UsuarioService usuarioService;

    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping("/users")
    public ResponseEntity<List<Usuario>> listarUsuarios() {
        List<Usuario> usuario = usuarioService.listarUsuarios();
        return ResponseEntity.ok(usuario);
    }

    @GetMapping("/users/{nome}")
    public ResponseEntity<List<UsuarioProjection>> listarUsuariosProjection(@PathVariable String nome) {
        List<UsuarioProjection> usuario = usuarioService.buscarListaProjetada(nome);
        return ResponseEntity.ok(usuario);
    }

    @PostMapping("/users")
    public ResponseEntity<Usuario> salvarUsuario(@RequestBody UsuarioPostRequestBody usuarioPostRequestBody){
        usuarioService.salvarUsuario(usuarioPostRequestBody);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
