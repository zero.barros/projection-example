package com.banzo.requests.usuario;


import com.banzo.requests.carro.CarroPostRequestBody;
import lombok.*;

import java.util.Date;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UsuarioPostRequestBody {

    private String firstName;

    private String lastName;

    private String email;

    private Date birthday;

    private String login;

    private String password;

    private String phone;

    private List<CarroPostRequestBody> cars;
}
