package com.banzo.requests.usuario;

import lombok.Builder;
import lombok.Getter;

import java.util.Date;

@Getter
@Builder
public class UsuarioModelResponse {

    private String firstName;

    private String lastName;

    private String email;

    private Date birthday;

    private String login;

    private String password;

    private String phone;
}
