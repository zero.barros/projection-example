package com.banzo.requests.carro;

public interface UsuarioProjection {

    String getFirstName();

    String getLastName();

    String getEmail();
}
