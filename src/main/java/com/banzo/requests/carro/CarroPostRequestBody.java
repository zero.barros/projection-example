package com.banzo.requests.carro;


import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class CarroPostRequestBody {

    private Integer year;

    private String licensePlate;

    private String model;

    private String color;

    private Integer codigoUsuario;
}
