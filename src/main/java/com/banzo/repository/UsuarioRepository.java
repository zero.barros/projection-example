package com.banzo.repository;

import com.banzo.domain.Usuario;
import com.banzo.requests.carro.UsuarioProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
    List<UsuarioProjection> getUsuarioByFirstName(String nome);
}
